package errors

const (
	OK uint = iota
	ErrorInput
	ErrorLoading
	ErrorExistence
	ErrorInconsistentData
	ErrorBanned
	ErrorValidation
	ErrorSaving
	ErrorForbidden
	ErrorCaptchaServer
	ErrorCaptchaWrong
	ErrorNotEnoughMoney
	ErrorSQL
)
