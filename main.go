package main

import (
	"bitbucket.org/fulleren/test-go-vue/config"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/mikespook/gorbac"
	"github.com/mtfelian/log"
	"github.com/robfig/cron"
	"net/http"
	"os"
)

// RBAC это указатель на объект, содержащий права доступа
var RBAC *gorbac.RBAC

func main() {
	// запуск лога
	var err error
	log.Log, err = log.InitLog()
	if err != nil {
		fmt.Sprintf("Failed to start logger %v\n", err)
		os.Exit(1)
		return
	}
	defer func() {
		log.Log.Flush()
		log.Log.Destroy()
	}()

	RBAC, err = initRBAC()
	if err != nil {
		fmt.Sprintf("Error init roles: %v\n", err)
		os.Exit(1)
		return
	}

	c := doCron()
	defer c.Stop()

	// запуск роутера
	router := Gin()
	fmt.Println("port", config.Port())
	router.Run(fmt.Sprintf(":%d", config.Port()))
}

// doCron запускает планировщик
func doCron() *cron.Cron {
	c := cron.New()
	addCronInitLog(c)

	c.Start()
	return c
}

// addCronInitLog добавляет задание по инициализации логирования
func addCronInitLog(c *cron.Cron) {
	c.AddFunc("@midnight", func() {
		if log.Log != nil {
			log.Log.Flush()
		}
		var err error
		log.Log, err = log.InitLog()
		if err != nil {
			fmt.Println("Ошибка перезапуска лога: " + err.Error())
			os.Exit(1)
			return
		}
	})
}

// corsGinMiddleware решает проблему доступа с CORS
func corsGinMiddleware(c *gin.Context) {
	fmt.Printf("%s %s: begin\n", c.Request.Method, c.Request.URL)
	// add header Access-Control-Allow-Origin
	c.Writer.Header().Add("Access-Control-Allow-Origin", "*")
	if c.Request.Method == http.MethodOptions {
		c.Abort()
		c.Writer.Header().Add("Access-Control-Allow-Methods", "GET, OPTIONS, POST, PUT, DELETE")
		c.Writer.Header().Add("Access-Control-Allow-Headers", "User-Token, Content-Type, Content-Length")
		c.Writer.Header().Add("Content-Length", "0")
		c.Writer.Header().Add("Content-Type", "text/plain")
		c.Status(http.StatusOK)
		fmt.Printf("%s %s: finished options block\n", c.Request.Method, c.Request.URL)
		return
	}
	fmt.Printf("%s %s: after options block\n", c.Request.Method, c.Request.URL)
	c.Next()
}

// Gin запускает роутер запросов
func Gin() *gin.Engine {
	gin.SetMode(gin.ReleaseMode)

	router := gin.Default()
	router.Use(corsGinMiddleware)

	router.GET("/ping", apiPing)

	// api.go
	router.POST("/login", apiLogin)
	router.POST("/token", apiToken)
	router.POST("/user", apiCreateUser)
	router.PUT("/user", apiEditUser)
	router.GET("/user", apiGetUser)
	router.GET("/users", apiGetUsers)
	router.DELETE("/user", apiDisableUser)
	router.PUT("/password", apiChangePassword)
	router.PUT("/email", apiChangeEmail)

	return router
}
