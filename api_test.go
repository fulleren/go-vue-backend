package main

import (
	"bitbucket.org/fulleren/test-go-vue/user"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/mihteh/types"
	"github.com/mtfelian/utils"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

// getValidApiLoginInput возвращает валидные входные данные для API входа по логину и паролю
func getValidApiLoginInput() apiLoginInput {
	return apiLoginInput{
		Email:    "ab@ab.ru",
		Password: "098f6bcd4621d373cade4e832627b4f6", // md5 от "test"
	}
}

func getValidApiTokenInput(token string) apiTokenInput {
	return apiTokenInput{Token: token}
}

// doAuthorizedRequest выполняет авторизованный HTTP-запрос
// (в заголовках HTTP присутствует поле для авторизации по токену)
func doAuthorizedRequest(token string, method string, url string, body io.Reader) (*http.Response, error) {
	request, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}
	request.Header.Add(authHeaderName, token)
	return http.DefaultClient.Do(request)
}

// doAuthorizedPostFile выполняет авторизованный HTTP-запрос
// (в заголовках HTTP присутствует поле для авторизации по токену)
func doAuthorizedPostFile(req utils.FileUploadRequest, token string) (*http.Response, error) {
	request, err := utils.NewFileUploadRequest(req)
	if err != nil {
		return nil, err
	}
	request.Header.Add(authHeaderName, token)
	return http.DefaultClient.Do(request)
}

// getValidUrlParamsForApiDisableUser возвращает валидные параметры URL для API метода apiDisableUser
func getValidUrlParamsForApiDisableUser(userId uint) url.Values {
	urlParams := url.Values{}
	urlParams.Add("id", fmt.Sprintf("%d", userId))
	urlParams.Add("ban_reason", fmt.Sprintf("%d", user.BanReasonAdministrative))
	return urlParams
}

// getValidApiChangePasswordInput возвращает валидные входные данные для API смены пароля
func getValidApiChangePasswordInput(u user.User) apiChangePasswordInput {
	return apiChangePasswordInput{
		Id:          u.Id,
		OldPassword: u.Password,
		NewPassword: "22af645d1859cb5ca6da0c484f1f37ea", // md5 от "new"
	}
}

// getValidApiChangeEmailInput возвращает валидные входные данные для API смены email
func getValidApiChangeEmailInput(u user.User) apiChangeEmailInput {
	return apiChangeEmailInput{
		Id:       u.Id,
		Password: u.Password,
		NewEmail: "abc@abc.ru",
	}
}

var _ = Describe("Checking API.", func() {
	AfterEach(func() {
		user.Cleanup()
	})
	BeforeEach(func() {
		user.Generate()
	})

	// TestApiPing проверяет обработчик ping-pong
	Describe("testing ApiPing", func() {
		It("checks ApiPing", func() {
			url := fmt.Sprintf("%s/ping", server.URL)
			response, err := http.Get(url)
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusOK))

			respBytes, err := ioutil.ReadAll(response.Body)
			Expect(err).NotTo(HaveOccurred())
			defer response.Body.Close()

			expectedResult := "pong"
			Expect(string(respBytes)).To(Equal(expectedResult))
		})
	})

	Describe("testing ApiLogin", func() {
		It("checks ApiLogin if OK", func() {
			input := getValidApiLoginInput()
			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/login", server.URL)
			response, err := http.Post(url, "application/json", bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusOK))

			respBytes, err := ioutil.ReadAll(response.Body)
			Expect(err).NotTo(HaveOccurred())
			defer response.Body.Close()

			var output apiLoginOutput
			Expect(json.Unmarshal(respBytes, &output)).To(Succeed())
			Expect(output.Id).To(Equal(user.TD.Users[0].Id))
			Expect(output.Token).ToNot(Equal(""))
		})

		It("checks ApiLogin if no body", func() {
			url := fmt.Sprintf("%s/login", server.URL)
			response, err := http.Post(url, "application/json", nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})

		It("checks ApiLogin if fake body", func() {
			url := fmt.Sprintf("%s/login", server.URL)
			response, err := http.Post(url, "application/json", strings.NewReader("fake"))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})

		It("checks ApiLogin if user not found", func() {
			input := getValidApiLoginInput()
			input.Email = "r0@uipef.ru"
			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/login", server.URL)
			response, err := http.Post(url, "application/json", bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})

		It("checks ApiLogin if bad password", func() {
			input := getValidApiLoginInput()
			input.Password = "wrong"
			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/login", server.URL)
			response, err := http.Post(url, "application/json", bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusForbidden))
		})

		It("checks ApiLogin if user banned", func() {
			user.TD.Users[0].Banned = true
			Expect(user.TD.Users[0].Save()).To(Succeed())

			input := getValidApiLoginInput()
			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/login", server.URL)
			response, err := http.Post(url, "application/json", bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusForbidden))
		})
	})

	Describe("testing ApiToken", func() {
		It("checks ApiToken if OK", func() {
			oldToken := *user.TD.Users[0].Token
			input := getValidApiTokenInput(oldToken)
			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/token", server.URL)
			response, err := http.Post(url, "application/json", bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusOK))

			respBytes, err := ioutil.ReadAll(response.Body)
			Expect(err).NotTo(HaveOccurred())
			defer response.Body.Close()

			var output apiTokenOutput
			Expect(json.Unmarshal(respBytes, &output)).To(Succeed())

			Expect(output.Id).To(Equal(user.TD.Users[0].Id))
			Expect(output.Token).NotTo(Equal(""))
			Expect(output.Token).To(Equal(oldToken))
			Expect(output.Role).To(Equal(user.TD.Users[0].Role))
			Expect(output.Banned).To(Equal(user.TD.Users[0].Banned))
		})

		It("checks ApiToken if invalid input", func() {
			oldToken := *user.TD.Users[0].Token
			input := getValidApiTokenInput(oldToken)
			input.LastIP = utils.PString("qqq")
			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/token", server.URL)
			response, err := http.Post(url, "application/json", bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusInternalServerError))
		})

		It("checks ApiToken if no body", func() {
			url := fmt.Sprintf("%s/token", server.URL)
			response, err := http.Post(url, "application/json", nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})

		It("checks ApiToken if fake body", func() {
			url := fmt.Sprintf("%s/token", server.URL)
			response, err := http.Post(url, "application/json", strings.NewReader("fake"))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})

		It("checks ApiToken if user not found", func() {
			input := getValidApiTokenInput(*user.TD.Users[0].Token)
			input.Token = "qq"
			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/token", server.URL)
			response, err := http.Post(url, "application/json", bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})

		It("checks ApiToken if user banned", func() {
			user.TD.Users[0].Banned = true
			Expect(user.TD.Users[0].Save()).To(Succeed())

			input := getValidApiTokenInput(*user.TD.Users[0].Token)
			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/token", server.URL)
			response, err := http.Post(url, "application/json", bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusForbidden))
		})
	})

	Describe("testing ApiCreateUser", func() {
		It("checks ApiCreateUser if OK", func() {
			input := apiCreateUserInput{
				User: user.GetValidUserMinimal(),
			}
			input.Email = "newemail@ab.ru" // отличается от email в тестовых данных
			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/user", server.URL)
			response, err := http.Post(url, "application/json", bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusCreated))

			respBytes, err := ioutil.ReadAll(response.Body)
			Expect(err).NotTo(HaveOccurred())
			defer response.Body.Close()

			var output apiCreateUserOutput
			Expect(json.Unmarshal(respBytes, &output)).To(Succeed())

			Expect(output.Id).To(BeNumerically(">=", 1))
			Expect(output.Token).NotTo(Equal(""))

			createdUser, err := user.LoadByEmail(input.Email)
			Expect(err).NotTo(HaveOccurred())
			Expect(createdUser).NotTo(BeNil())
			Expect(*createdUser.Token).To(Equal(output.Token))
		})

		It("checks ApiCreateUser if no body", func() {
			url := fmt.Sprintf("%s/user", server.URL)
			response, err := http.Post(url, "application/json", nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})

		It("checks ApiCreateUser if fake body", func() {
			url := fmt.Sprintf("%s/user", server.URL)
			response, err := http.Post(url, "application/json", strings.NewReader("fake"))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})

		It("checks ApiCreateUser if invalid input", func() {
			input := apiCreateUserInput{
				User: user.GetValidUserMinimal(),
			}
			input.Password = ""
			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/user", server.URL)
			response, err := http.Post(url, "application/json", bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})

		It("checks ApiCreateUser if already exists", func() {
			input := apiCreateUserInput{
				User: user.GetValidUserMinimal(),
			}
			input.Email = user.TD.Users[0].Email
			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/user", server.URL)
			response, err := http.Post(url, "application/json", bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})
	})

	Describe("testing ApiEditUser", func() {
		It("checks ApiEditUser if OK", func() {
			input := user.GetValidUserMinimal()
			input.Id = user.TD.Users[0].Id          // редактируем юзера 0
			input.Banned = !user.TD.Users[0].Banned // проверка что не изменится
			passwordBefore := user.TD.Users[0].Password
			input.Password = fmt.Sprintf("1010%s", user.TD.Users[0].Password)[:32] // проверка что не изменится
			input.Email = fmt.Sprintf("%s11", user.TD.Users[0].Email)              // проверка что не изменится

			expectedFirstName := "Гавриил"
			input.FirstName = utils.PString(expectedFirstName)

			input.BirthDate = types.ToDate(time.Now().AddDate(-30, -5, -79)).Nullable()
			expectedBirthDayString := input.BirthDate.String()

			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			address := fmt.Sprintf("%s/user", server.URL)
			token := *user.TD.Users[0].Token // авторизация как юзер 0
			response, err := doAuthorizedRequest(token, http.MethodPut, address, bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusOK))

			respBytes, err := ioutil.ReadAll(response.Body)
			Expect(err).NotTo(HaveOccurred())
			defer response.Body.Close()

			var output apiEditUserOutput
			Expect(json.Unmarshal(respBytes, &output)).To(Succeed())

			Expect(output.Id).To(Equal(input.Id))
			Expect(output.Token).ToNot(Equal(""))

			// теперь получаем пользователя и проверяем на соответствие тому что мы хотели установить

			var changedUser user.User
			urlParams := url.Values{}
			urlParams.Add("id", fmt.Sprintf("%d", input.Id))
			address = fmt.Sprintf("%s/user?%s", server.URL, urlParams.Encode())
			response, err = doAuthorizedRequest(output.Token, http.MethodGet, address, nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusOK))
			respBytes, err = ioutil.ReadAll(response.Body)
			Expect(err).NotTo(HaveOccurred())
			defer response.Body.Close()
			Expect(json.Unmarshal(respBytes, &changedUser)).To(Succeed())
			Expect(changedUser.Id).To(Equal(input.Id))

			Expect(changedUser).NotTo(BeNil())
			Expect(changedUser.Id).To(Equal(input.Id))
			Expect(changedUser.Banned).To(Equal(user.TD.Users[0].Banned))
			Expect(changedUser.Email).To(Equal(user.TD.Users[0].Email))
			Expect(changedUser.FirstName).NotTo(BeNil())
			Expect(*changedUser.FirstName).To(Equal(expectedFirstName))
			Expect(changedUser.BirthDate.String()).To(Equal(expectedBirthDayString))

			err = user.TD.Users[0].Reload()
			Expect(err).NotTo(HaveOccurred())
			Expect(*user.TD.Users[0].Token).To(Equal(output.Token))
			Expect(user.TD.Users[0].Password).To(Equal(passwordBefore))
		})

		It("checks ApiEditUser if no body", func() {
			url := fmt.Sprintf("%s/user", server.URL)
			token := *user.TD.Users[0].Token
			response, err := doAuthorizedRequest(token, http.MethodPut, url, nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})

		It("checks ApiEditUser if fake body", func() {
			url := fmt.Sprintf("%s/user", server.URL)
			token := *user.TD.Users[0].Token
			response, err := doAuthorizedRequest(token, http.MethodPut, url, strings.NewReader("fake"))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})

		It("checks ApiEditUser if invalid input", func() {
			input := user.GetValidUserMinimal()
			input.Id = user.TD.Users[0].Id
			input.FirstName = utils.PString(strings.Repeat("a", 31))

			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/user", server.URL)
			token := *user.TD.Users[0].Token
			response, err := doAuthorizedRequest(token, http.MethodPut, url, bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusInternalServerError))
		})

		It("checks ApiEditUser if not exists", func() {
			input := user.GetValidUserMinimal()
			input.Id = 100500
			input.FirstName = utils.PString("Artur")

			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/user", server.URL)
			token := *user.TD.Users[0].Token
			response, err := doAuthorizedRequest(token, http.MethodPut, url, bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusForbidden))
		})

		It("checks ApiEditUser if not enough privileges", func() {
			input := user.GetValidUserMinimal()
			input.Id = user.TD.Users[1].Id // редактируем юзера 1
			input.FirstName = utils.PString("Artur")

			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/user", server.URL)
			token := *user.TD.Users[0].Token // авторизуемся как юзер 0
			response, err := doAuthorizedRequest(token, http.MethodPut, url, bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusForbidden))
		})

		It("checks ApiEditUser id admin edits other", func() {
			input := user.GetValidUserMinimal()
			input.Id = user.TD.Users[0].Id // редактируем юзера 0
			input.FirstName = utils.PString("Artur")

			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/user", server.URL)
			token := *user.TD.Users[1].Token // авторизуемся как юзер 1
			response, err := doAuthorizedRequest(token, http.MethodPut, url, bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusOK))
		})

		It("checks ApiEditUser if not authorized", func() {
			input := user.GetValidUserMinimal()
			input.Id = user.TD.Users[0].Id

			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/user", server.URL)
			token := strings.Repeat("t", 32)
			response, err := doAuthorizedRequest(token, http.MethodPut, url, bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusForbidden))
		})
	})

	Describe("testing ApiGetUser", func() {
		It("checks ApiGetUser if OK", func() {
			urlParams := url.Values{}
			requestedUserId := user.TD.Users[0].Id
			urlParams.Add("id", fmt.Sprintf("%d", requestedUserId))
			url := fmt.Sprintf("%s/user?%s", server.URL, urlParams.Encode())
			token := *user.TD.Users[1].Token // смотрим под админом
			response, err := doAuthorizedRequest(token, http.MethodGet, url, nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusOK))

			respBytes, err := ioutil.ReadAll(response.Body)
			Expect(err).NotTo(HaveOccurred())
			defer response.Body.Close()

			var output user.User
			Expect(json.Unmarshal(respBytes, &output)).To(Succeed())

			Expect(output.Id).To(Equal(requestedUserId))
			equal, diff := user.CompareExceptSomeFields(output, user.TD.Users[0])
			Expect(equal).To(BeTrue(), "Сохранённая и загруженная модель отличаются: \n%s", diff)
		})

		It("checks ApiGetUser if not authorized", func() {
			urlParams := url.Values{}
			requestedUserId := user.TD.Users[0].Id
			urlParams.Add("id", fmt.Sprintf("%d", requestedUserId))
			url := fmt.Sprintf("%s/user?%s", server.URL, urlParams.Encode())
			token := strings.Repeat("t", 32)
			response, err := doAuthorizedRequest(token, http.MethodGet, url, nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusForbidden))
		})

		It("checks ApiGetUser if wrong url params", func() {
			url := fmt.Sprintf("%s/user?%s", server.URL, "")
			token := *user.TD.Users[0].Token
			response, err := doAuthorizedRequest(token, http.MethodGet, url, nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})

		It("checks ApiGetUser if not exists", func() {
			urlParams := url.Values{}
			urlParams.Add("id", fmt.Sprintf("%d", 100500))
			url := fmt.Sprintf("%s/user?%s", server.URL, urlParams.Encode())
			token := *user.TD.Users[0].Token
			response, err := doAuthorizedRequest(token, http.MethodGet, url, nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})
	})

	Describe("testing ApiGetUsers", func() {
		It("checks ApiGetUsers if OK", func() {
			urlParams := url.Values{}
			urlParams.Add("from", fmt.Sprintf("%d", 1))
			urlParams.Add("to", fmt.Sprintf("%d", 100500))
			url := fmt.Sprintf("%s/users?%s", server.URL, urlParams.Encode())
			token := *user.TD.Users[1].Token // смотрим под админом
			response, err := doAuthorizedRequest(token, http.MethodGet, url, nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusOK))

			respBytes, err := ioutil.ReadAll(response.Body)
			Expect(err).NotTo(HaveOccurred())
			defer response.Body.Close()

			var output user.Users
			Expect(json.Unmarshal(respBytes, &output)).To(Succeed())

			Expect(len(user.TD.Users)).To(Equal(len(output)))
		})

		It("checks ApiGetUsers if not authorized", func() {
			urlParams := url.Values{}
			urlParams.Add("from", fmt.Sprintf("%d", 1))
			urlParams.Add("to", fmt.Sprintf("%d", 100500))
			url := fmt.Sprintf("%s/user?%s", server.URL, urlParams.Encode())
			token := strings.Repeat("t", 32)
			response, err := doAuthorizedRequest(token, http.MethodGet, url, nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusForbidden))
		})

		It("checks ApiGetUsers OK if no url params", func() {
			url := fmt.Sprintf("%s/users?%s", server.URL, "")
			token := *user.TD.Users[0].Token
			response, err := doAuthorizedRequest(token, http.MethodGet, url, nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusOK))

			respBytes, err := ioutil.ReadAll(response.Body)
			Expect(err).NotTo(HaveOccurred())
			defer response.Body.Close()

			var output user.Users
			Expect(json.Unmarshal(respBytes, &output)).To(Succeed())

			Expect(len(user.TD.Users)).To(Equal(len(output)))
		})
	})

	Describe("testing ApiDisableUser", func() {
		It("checks ApiDisableUser if OK", func() {
			requestedUserId := user.TD.Users[0].Id
			urlParams := getValidUrlParamsForApiDisableUser(requestedUserId)
			url := fmt.Sprintf("%s/user?%s", server.URL, urlParams.Encode())
			token := *user.TD.Users[1].Token // под админом
			response, err := doAuthorizedRequest(token, http.MethodDelete, url, nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusOK))

			loadedUser, err := user.LoadById(requestedUserId)
			Expect(err).NotTo(HaveOccurred())
			Expect(loadedUser.Banned).ToNot(Equal(user.TD.Users[0].Banned))
		})

		It("checks ApiDisableUser if not enough privileges", func() {
			requestedUserId := user.TD.Users[0].Id
			urlParams := getValidUrlParamsForApiDisableUser(requestedUserId)
			url := fmt.Sprintf("%s/user?%s", server.URL, urlParams.Encode())
			token := *user.TD.Users[0].Token // под пользователем
			response, err := doAuthorizedRequest(token, http.MethodDelete, url, nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusForbidden))

			loadedUser, err := user.LoadById(requestedUserId)
			Expect(err).NotTo(HaveOccurred())
			Expect(loadedUser.Banned).To(Equal(user.TD.Users[0].Banned))
		})

		It("checks ApiDisableUser if not authorized", func() {
			urlParams := getValidUrlParamsForApiDisableUser(user.TD.Users[0].Id)
			url := fmt.Sprintf("%s/user?%s", server.URL, urlParams.Encode())
			token := strings.Repeat("t", 32)
			response, err := doAuthorizedRequest(token, http.MethodDelete, url, nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusForbidden))
		})

		It("checks ApiDisableUser if empty url params", func() {
			url := fmt.Sprintf("%s/user?%s", server.URL, "")
			token := *user.TD.Users[1].Token
			response, err := doAuthorizedRequest(token, http.MethodDelete, url, nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})

		It("checks ApiDisableUser if wrong ban reason", func() {
			urlParams := getValidUrlParamsForApiDisableUser(user.TD.Users[0].Id)
			urlParams.Set("ban_reason", fmt.Sprintf("%d", user.BanReasonAdministrative+1))
			url := fmt.Sprintf("%s/user?%s", server.URL, urlParams.Encode())
			token := *user.TD.Users[1].Token
			response, err := doAuthorizedRequest(token, http.MethodDelete, url, nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})

		It("checks ApiDisableUser if not exists", func() {
			urlParams := url.Values{}
			urlParams.Add("id", fmt.Sprintf("%d", 100500))
			url := fmt.Sprintf("%s/user?%s", server.URL, urlParams.Encode())
			token := *user.TD.Users[1].Token
			response, err := doAuthorizedRequest(token, http.MethodDelete, url, nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})
	})

	Describe("testing ApiChangePassword", func() {
		It("checks ApiChangePassword if OK", func() {
			input := getValidApiChangePasswordInput(user.TD.Users[0])
			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/password", server.URL)
			token := *user.TD.Users[0].Token
			response, err := doAuthorizedRequest(token, http.MethodPut, url, bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusOK))

			respBytes, err := ioutil.ReadAll(response.Body)
			Expect(err).NotTo(HaveOccurred())
			defer response.Body.Close()

			var output apiChangePasswordOutput
			Expect(json.Unmarshal(respBytes, &output)).To(Succeed())

			Expect(output.Id).To(Equal(input.Id))
			Expect(output.Token).ToNot(Equal(""))

			changedUser, err := user.LoadById(input.Id)
			Expect(err).NotTo(HaveOccurred())
			Expect(changedUser).NotTo(BeNil())
			Expect(*changedUser.Token).To(Equal(output.Token))
			Expect(changedUser.Password).To(Equal(input.NewPassword))
		})

		It("checks ApiChangePassword if not enough privileges", func() {
			input := getValidApiChangePasswordInput(user.TD.Users[1]) // менять будем админу
			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/password", server.URL)
			token := *user.TD.Users[0].Token // авторизуемся под пользователем
			response, err := doAuthorizedRequest(token, http.MethodPut, url, bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusForbidden))
		})

		It("checks ApiChangePassword if not authorized", func() {
			input := getValidApiChangePasswordInput(user.TD.Users[0])
			input.Id = user.TD.Users[0].Id

			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/password", server.URL)
			token := strings.Repeat("t", 32)
			response, err := doAuthorizedRequest(token, http.MethodPut, url, bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusForbidden))
		})

		It("checks ApiChangePassword if no body", func() {
			url := fmt.Sprintf("%s/password", server.URL)
			token := *user.TD.Users[0].Token
			response, err := doAuthorizedRequest(token, http.MethodPut, url, nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})

		It("checks ApiChangePassword if fake body", func() {
			url := fmt.Sprintf("%s/password", server.URL)
			token := *user.TD.Users[0].Token
			response, err := doAuthorizedRequest(token, http.MethodPut, url, strings.NewReader("fake"))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})

		It("checks ApiChangePassword if invalid input", func() {
			input := getValidApiChangePasswordInput(user.TD.Users[0])
			input.NewPassword = "qqq"

			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/password", server.URL)
			token := *user.TD.Users[0].Token
			response, err := doAuthorizedRequest(token, http.MethodPut, url, bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusInternalServerError))
		})

		It("checks ApiChangePassword if wrong old password", func() {
			input := getValidApiChangePasswordInput(user.TD.Users[0])
			input.OldPassword = input.NewPassword

			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/password", server.URL)
			token := *user.TD.Users[0].Token
			response, err := doAuthorizedRequest(token, http.MethodPut, url, bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})

		It("checks ApiChangePassword if not exists", func() {
			input := getValidApiChangePasswordInput(user.TD.Users[0])
			input.Id = 100500

			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/password", server.URL)
			token := *user.TD.Users[0].Token
			response, err := doAuthorizedRequest(token, http.MethodPut, url, bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})
	})

	Describe("testing ApiChangeEmail", func() {
		It("checks ApiChangeEmail if OK", func() {
			input := getValidApiChangeEmailInput(user.TD.Users[0])
			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/email", server.URL)
			token := *user.TD.Users[0].Token
			response, err := doAuthorizedRequest(token, http.MethodPut, url, bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusOK))

			respBytes, err := ioutil.ReadAll(response.Body)
			Expect(err).NotTo(HaveOccurred())
			defer response.Body.Close()

			var output apiChangeEmailOutput
			Expect(json.Unmarshal(respBytes, &output)).To(Succeed())

			Expect(output.Id).To(Equal(input.Id))
			Expect(output.Token).ToNot(Equal(""))

			changedUser, err := user.LoadById(input.Id)
			Expect(err).NotTo(HaveOccurred())
			Expect(changedUser).NotTo(BeNil())
			Expect(*changedUser.Token).To(Equal(output.Token))
			Expect(changedUser.Email).To(Equal(input.NewEmail))
		})

		It("checks ApiChangeEmail if not enough privileges", func() {
			input := getValidApiChangeEmailInput(user.TD.Users[1]) // менять будем админу
			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/email", server.URL)
			token := *user.TD.Users[0].Token // авторизуемся как клиент
			response, err := doAuthorizedRequest(token, http.MethodPut, url, bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusForbidden))
		})

		It("checks ApiChangeEmail if not authorized", func() {
			input := getValidApiChangeEmailInput(user.TD.Users[0])
			input.Id = user.TD.Users[0].Id

			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/email", server.URL)
			token := strings.Repeat("t", 32)
			response, err := doAuthorizedRequest(token, http.MethodPut, url, bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusForbidden))
		})

		It("checks ApiChangeEmail if no body", func() {
			url := fmt.Sprintf("%s/email", server.URL)
			token := *user.TD.Users[0].Token
			response, err := doAuthorizedRequest(token, http.MethodPut, url, nil)
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})

		It("checks ApiChangeEmail if fake body", func() {
			url := fmt.Sprintf("%s/email", server.URL)
			token := *user.TD.Users[0].Token
			response, err := doAuthorizedRequest(token, http.MethodPut, url, strings.NewReader("fake"))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})

		It("checks ApiChangeEmail if invalid input", func() {
			input := getValidApiChangeEmailInput(user.TD.Users[0])
			input.NewEmail = "a@a@.ru"

			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/email", server.URL)
			token := *user.TD.Users[0].Token
			response, err := doAuthorizedRequest(token, http.MethodPut, url, bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusInternalServerError))
		})

		It("checks ApiChangeEmail if wrong password", func() {
			input := getValidApiChangeEmailInput(user.TD.Users[0])
			input.Password = "22af645d1859cb5ca6da0c484f1f37ea" // md5 от "new"

			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/email", server.URL)
			token := *user.TD.Users[0].Token
			response, err := doAuthorizedRequest(token, http.MethodPut, url, bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})

		It("checks ApiChangeEmail if not exists", func() {
			input := getValidApiChangeEmailInput(user.TD.Users[0])
			input.Id = 100500

			jsonByte, err := json.Marshal(input)
			Expect(err).NotTo(HaveOccurred())

			url := fmt.Sprintf("%s/email", server.URL)
			token := *user.TD.Users[0].Token
			response, err := doAuthorizedRequest(token, http.MethodPut, url, bytes.NewReader(jsonByte))
			Expect(err).NotTo(HaveOccurred())
			Expect(response.StatusCode).To(Equal(http.StatusBadRequest))
		})
	})
})
