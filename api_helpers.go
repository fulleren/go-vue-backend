package main

import (
	. "bitbucket.org/fulleren/test-go-vue/errors"
	u "bitbucket.org/fulleren/test-go-vue/user"
	"encoding/json"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/mikespook/gorbac"
	"github.com/mtfelian/log"
	"github.com/mtfelian/utils"
	"io/ioutil"
	"net/http"
)

// checkAuthorization проверяет наличие пользователя с неистёкшим токеном,
// переданным в заголовке запроса, а так же, доступно ли с его правами
// выполнение метода.
// Возвращает:
// 1. пользователя если он существует и права позволяют выполнение, иначе nil
// 2. ошибку или nil
func checkAuthorization(request *http.Request) (*u.User, error) {
	token := request.Header.Get(authHeaderName)
	userToCheck, err := u.LoadByToken(token)
	if err != nil {
		return nil, err
	}

	// пользователь не найден
	if userToCheck == nil {
		return nil, errors.New("Wrong token")
	}

	if userToCheck.Banned {
		return nil, errors.New("User is banned")
	}

	apiMethodName, err := utils.CallerFuncName()
	if err != nil {
		return nil, err
	}

	permission := gorbac.NewStdPermission(apiMethodName)
	if !RBAC.IsGranted(userToCheck.Role, permission, nil) {
		return nil, errors.New("Forbidden")
	}

	return userToCheck, nil
}

// readAndUnmarshal читает тело HTTP запроса и пытается разобрать JSON из него в объект to
// Возвращает срез байт тела из запроса и ошибку/nil
func readAndUnmarshal(c *gin.Context, to interface{}) ([]byte, error) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		return body, err
	}

	return body, json.Unmarshal(body, to)
}

// tryToLoadUser пытается загрузить пользователя по его id,
// возвращает указатель на пользователя и ошибку/nil,
// Параметры: c - контекст Gin, userId - id пользователя, requestBody - срез байт тела запроса
func tryToLoadUser(c *gin.Context, userId uint, requestBody []byte) (*u.User, error) {
	user, err := u.LoadById(userId)
	if err != nil {
		log.Log.ReturnError(c, http.StatusInternalServerError, ErrorLoading, err.Error(), requestBody)
		return user, err
	}
	if user == nil {
		log.Log.ReturnError(c, http.StatusBadRequest, ErrorExistence, "User not found", requestBody)
		return user, errors.New("User not found")
	}
	return user, nil
}
