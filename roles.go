package main

import (
	"bitbucket.org/fulleren/test-go-vue/user"
	"fmt"
	"github.com/mikespook/gorbac"
	"reflect"
	"runtime"
)

// allRoles возвращает отображение всех строковых констант ролей в роли RBAC
func allRoles() map[string]*gorbac.StdRole {
	roles := []string{user.RoleAdmin, user.RoleClient, user.RoleSystem}
	gorbacRoles := make(map[string]*gorbac.StdRole)
	for _, role := range roles {
		gorbacRoles[role] = gorbac.NewStdRole(role)
	}
	return gorbacRoles
}

// functionsToNames преобразует функции в строки, содержащие их имена
func functionsToNames(actions ...interface{}) []string {
	results := make([]string, 0, len(actions))
	for _, m := range actions {
		if kind := reflect.TypeOf(m).Kind(); kind != reflect.Func {
			panic(fmt.Sprintf("Not a function: %v", m))
		}
		funcName := runtime.FuncForPC(reflect.ValueOf(m).Pointer()).Name()
		results = append(results, funcName)
	}
	return results
}

// InitRBAC инициализирует роли и права доступа
func initRBAC() (*gorbac.RBAC, error) {
	rbac := gorbac.New()

	// добавляем в RBAC роли, созданные на основе строк, их отображающих
	roles := allRoles()
	// roleRBAC содержит роль RBAC
	for _, roleRBAC := range roles {
		if err := rbac.Add(roleRBAC); err != nil {
			return rbac, nil
		}
	}

	// админу даём так же все права игрока (права админа наследуются от прав игрока)
	if err := rbac.SetParent(user.RoleAdmin, user.RoleClient); err != nil {
		return rbac, nil
	}
	// права системы наследуются от прав админа (те же + ... )
	if err := rbac.SetParent(user.RoleSystem, user.RoleAdmin); err != nil {
		return rbac, nil
	}

	clientRules := []interface{}{
		apiGetUser, apiGetUsers, apiEditUser, apiChangePassword, apiChangeEmail,
	}
	adminRules := []interface{}{
		apiDisableUser,
	}
	systemRules := []interface{}{}

	// roleString содержит строковую константу роли
	for roleString, _ := range roles {
		switch roleString {
		case user.RoleClient:
			clientRulesStrings := functionsToNames(clientRules...)
			for _, rule := range clientRulesStrings {
				roles[roleString].Assign(gorbac.NewStdPermission(rule))
			}
		case user.RoleAdmin:
			adminRulesStrings := functionsToNames(adminRules...)
			for _, rule := range adminRulesStrings {
				roles[roleString].Assign(gorbac.NewStdPermission(rule))
			}
		case user.RoleSystem:
			systemRulesString := functionsToNames(systemRules...)
			for _, rule := range systemRulesString {
				roles[roleString].Assign(gorbac.NewStdPermission(rule))
			}
		}
	}

	return rbac, nil
}
