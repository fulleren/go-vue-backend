package main

import (
	. "bitbucket.org/fulleren/test-go-vue/errors"
	u "bitbucket.org/fulleren/test-go-vue/user"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/mihteh/types"
	"github.com/mtfelian/cli"
	"github.com/mtfelian/log"
	"github.com/mtfelian/utils"
	"net/http"
	"regexp"
	"strings"
)

// authHeaderName имя заголовка HTTP, содержащего токен
const authHeaderName = "User-Token"

// apiPing обработчик, возвращающий "pong"
func apiPing(c *gin.Context) {
	c.String(http.StatusOK, "pong")
}

// apiLoginInput данные, которые принимает метод apiLogin
type apiLoginInput struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// apiLoginOutput данные, которые возвращает метод apiLogin
type apiLoginOutput struct {
	Id    uint   `json:"id"`
	Token string `json:"token"`
}

// apiLogin обработчик авторизации по логину и паролю
func apiLogin(c *gin.Context) {
	var inputData apiLoginInput
	body, err := readAndUnmarshal(c, &inputData)
	if err != nil {
		log.Log.ReturnError(c, http.StatusBadRequest, ErrorInput, err.Error(), body)
		return
	}

	fmt.Println(">>", inputData)

	user, err := u.LoadByEmail(inputData.Email)
	if err != nil {
		log.Log.ReturnError(c, http.StatusInternalServerError, ErrorLoading, err.Error(), body)
		return
	}

	if user == nil {
		log.Log.ReturnError(c, http.StatusBadRequest, ErrorExistence, "User not exists", body)
		return
	}

	if user.Password != inputData.Password {
		log.Log.ReturnError(c, http.StatusForbidden, ErrorInconsistentData, "Wrong password", body)
		return
	}

	if user.Banned {
		log.Log.ReturnError(c, http.StatusForbidden, ErrorBanned, "This user is banned", body)
		return
	}

	isValid, vContext := user.Validate()
	if !isValid {
		log.Log.ReturnError(c, http.StatusBadRequest, ErrorValidation,
			fmt.Sprintf("Validation error: %s\n", vContext), body)
		return
	}

	user.LastIP = utils.PString(utils.GetIPAddress(c.Request))
	user.SetLastActiveDate()
	user.GenerateToken()
	if err := user.Save(); err != nil {
		log.Log.ReturnError(c, http.StatusInternalServerError, err.Code(), err.Message(), body)
		return
	}

	c.JSON(http.StatusOK, apiLoginOutput{
		Id:    user.Id,
		Token: *user.Token,
	})
}

// apiTokenInput данные, которые принимает метод apiToken
type apiTokenInput struct {
	Token  string  `json:"token"`
	LastIP *string `json:"lastIp,omitempty"`
}

// apiTokenOutput данные, которые возвращает метод apiToken
type apiTokenOutput struct {
	Id     uint   `json:"id"`
	Token  string `json:"token"`
	Role   string `json:"role"`
	Banned bool   `json:"bool"`
}

// apiToken обработчик авторизации по токену доступа
func apiToken(c *gin.Context) {
	var inputData apiTokenInput
	body, err := readAndUnmarshal(c, &inputData)
	if err != nil {
		log.Log.ReturnError(c, http.StatusBadRequest, ErrorInput, err.Error(), body)
		return
	}

	user, err := u.LoadByToken(inputData.Token)
	if err != nil {
		log.Log.ReturnError(c, http.StatusInternalServerError, ErrorLoading, err.Error(), body)
		return
	}
	if user == nil {
		log.Log.ReturnError(c, http.StatusBadRequest, ErrorExistence, "User not exists", body)
		return
	}

	if user.Token == nil || *user.Token != inputData.Token {
		log.Log.ReturnError(c, http.StatusInternalServerError, ErrorInconsistentData,
			"Token strangely invalid", body)
		return
	}

	if user.Banned {
		log.Log.ReturnError(c, http.StatusForbidden, ErrorBanned, "This user is banned", body)
		return
	}

	if inputData.LastIP != nil {
		user.LastIP = inputData.LastIP
	}

	user.SetLastActiveDate()
	user.ProlongToken()
	if err := user.VSave(); err != nil {
		log.Log.ReturnError(c, http.StatusInternalServerError, err.Code(), err.Message(), body)
		return
	}

	c.JSON(http.StatusOK, apiTokenOutput{
		Id:     user.Id,
		Token:  *user.Token,
		Role:   user.Role,
		Banned: user.Banned,
	})
}

// apiCreateUserInput данные, которые принимает метод apiCreateUser
type apiCreateUserInput struct {
	u.User
}

// apiCreateUserOutput данные, которые возвращает метод apiCreateUser
type apiCreateUserOutput struct {
	apiLoginOutput
}

// apiCreateUser обработчик регистрации (создания пользователя)
func apiCreateUser(c *gin.Context) {
	var inputData apiCreateUserInput

	body, err := readAndUnmarshal(c, &inputData)
	if err != nil {
		log.Log.ReturnError(c, http.StatusBadRequest, ErrorInput, err.Error(), body)
		return
	}

	user := inputData.User
	isValid, vContext := user.Validate()
	if !isValid {
		log.Log.ReturnError(c, http.StatusBadRequest, ErrorValidation,
			fmt.Sprintf("Validation error: %s\n", vContext), body)
		return
	}

	userExists, err := u.IsExistsByEmail(user.Email)
	if err != nil {
		log.Log.ReturnError(c, http.StatusInternalServerError, ErrorLoading, err.Error(), body)
		return
	}
	if userExists {
		log.Log.ReturnError(c, http.StatusBadRequest, ErrorExistence, "User already exists", body)
		return
	}

	// создание пользователя в БД

	// todo получение UTM из хедеров
	user.Role = u.RoleClient
	user.LastIP = utils.PString(utils.GetIPAddress(c.Request))
	user.SetLastActiveDate()
	user.GenerateToken()
	if err := user.Save(); err != nil {
		log.Log.ReturnError(c, http.StatusInternalServerError, err.Code(), err.Message(), body)
		return
	}

	c.JSON(http.StatusCreated, apiCreateUserOutput{
		apiLoginOutput: apiLoginOutput{
			Id:    user.Id,
			Token: *user.Token,
		},
	})
}

// apiEditUserOutput данные, которые возвращает метод apiEditUser
type apiEditUserOutput apiLoginOutput

// apiEditUser обработчик редактирования данных пользователя
// метод не изменяет следующие поля: id, email, password, banned
func apiEditUser(c *gin.Context) {
	authorizingUser, err := checkAuthorization(c.Request)
	if err != nil || authorizingUser == nil {
		log.Log.ReturnError(c, http.StatusForbidden, ErrorForbidden, fmt.Sprintf("%v", err), nil)
		return
	}

	var replacement u.User
	body, err := readAndUnmarshal(c, &replacement)
	if err != nil {
		log.Log.ReturnError(c, http.StatusBadRequest, ErrorInput, err.Error(), body)
		return
	}

	if !authorizingUser.CanDoWithOther(replacement.Id) {
		log.Log.ReturnError(c, http.StatusForbidden, ErrorForbidden, "Not enough privileges", body)
		return
	}

	user, err := tryToLoadUser(c, replacement.Id, body)
	if err != nil {
		return
	}

	user.GenerateToken()
	if err := user.UpdateWith(replacement, true); err != nil {
		log.Log.ReturnError(c, http.StatusInternalServerError, ErrorSaving, err.Error(), body)
		return
	}

	c.JSON(http.StatusOK, apiEditUserOutput{
		Id:    user.Id,
		Token: *user.Token,
	})
}

// apiGetUsersInput описывает входные данные для получения списка пользователей
type apiGetUsersInput struct {
	From           uint   `json:"from"`
	To             uint   `json:"to"`
	Order          string `json:"order"`
	OrderDirection string `json:"orderDirection"`
}

// decodeUrl получает входные данные из url-параметров
func (input *apiGetUsersInput) decodeUrl(c *gin.Context) error {
	var err error

	sFrom := c.DefaultQuery("from", "1")
	input.From, err = utils.StringToUint(sFrom)
	if err != nil || input.From < 1 {
		return fmt.Errorf("Error %v. Invalid 'from': %s", err, sFrom)
	}

	sTo := c.DefaultQuery("to", fmt.Sprintf("%d", input.From+20))
	input.To, err = utils.StringToUint(sTo)
	if err != nil || input.To <= input.From {
		return fmt.Errorf("Error %v. Invalid 'to': %s", err, sTo)
	}

	input.Order = c.DefaultQuery("order", "id")
	input.Order = regexp.MustCompile(`[^\w\._ ]`).ReplaceAllString(input.Order, "")

	input.OrderDirection = c.DefaultQuery("orderDirection", u.OrderDirectionDescending)
	input.OrderDirection = strings.TrimSpace(strings.ToUpper(input.OrderDirection))
	if input.OrderDirection != u.OrderDirectionAscending && input.OrderDirection != u.OrderDirectionDescending {
		input.OrderDirection = u.OrderDirectionDescending
	}
	return nil
}

// apiGetUsersOutput описывает выходные данные списка пользователей
type apiGetUsersOutput u.Users

// apiGetUsers обработчик получения списка пользователей
func apiGetUsers(c *gin.Context) {
	authorizingUser, err := checkAuthorization(c.Request)
	if err != nil || authorizingUser == nil {
		log.Log.ReturnError(c, http.StatusForbidden, ErrorForbidden, fmt.Sprintf("%v", err), nil)
		return
	}

	var params apiGetUsersInput
	if err := params.decodeUrl(c); err != nil {
		log.Log.ReturnError(c, http.StatusBadRequest, ErrorInput, err.Error(), nil)
		return
	}

	users, err := u.LoadList(params.From, params.To)
	if err != nil {
		log.Log.ReturnError(c, http.StatusBadRequest, ErrorLoading, err.Error(), nil)
		return
	}

	// убираем лишние данные
	for i := range users {
		users[i].Password = ""
		users[i].Token = nil
		// если тот кто запрашивает - не админ, опускаем ещё часть полей
		if !authorizingUser.IsAdmin() {
			users[i].TokenExpires = types.MakeNullDateTime()
			users[i].UTM = nil
			users[i].LastIP = nil
			// если не админ смотрит другого игрока, скрываем поля которые ему не нужно знать
			if authorizingUser.Id != users[i].Id {
				users[i].BirthDate = types.MakeNullDate()
				users[i].Phone = nil

				users[i].Address = nil
				users[i].Flat = nil
				users[i].Building = nil
				users[i].House = nil
				users[i].Street = nil
				users[i].PostCode = nil

				users[i].SessionMins = 0
			}
		}
	}

	cli.Println("{YAPI returned with length %d{0", len(users))

	c.JSON(http.StatusOK, users)
}

// apiGetUser обработчик получения данных пользователя
func apiGetUser(c *gin.Context) {
	authorizingUser, err := checkAuthorization(c.Request)
	if err != nil || authorizingUser == nil {
		log.Log.ReturnError(c, http.StatusForbidden, ErrorForbidden, fmt.Sprintf("%v", err), nil)
		return
	}

	id, err := utils.StringToUint(c.Query("id"))
	if err != nil {
		log.Log.ReturnError(c, http.StatusBadRequest, ErrorInput, err.Error(), nil)
		return
	}

	user, err := tryToLoadUser(c, id, nil)
	if err != nil {
		return
	}

	// следующие поля в выводе опускаем
	user.Password = ""
	user.Token = nil

	// если тот кто запрашивает - не админ, опускаем ещё часть полей
	if !authorizingUser.IsAdmin() {
		user.TokenExpires = types.MakeNullDateTime()
		user.UTM = nil
		user.LastIP = nil
		// если не админ смотрит другого игрока, скрываем поля которые ему не нужно знать
		if authorizingUser.Id != user.Id {
			user.BirthDate = types.MakeNullDate()
			user.Phone = nil

			user.Address = nil
			user.Flat = nil
			user.Building = nil
			user.House = nil
			user.Street = nil
			user.PostCode = nil

			user.SessionMins = 0
		}
	}

	c.JSON(http.StatusOK, user)
}

// apiDisableUser обработчик блокировки пользователя
func apiDisableUser(c *gin.Context) {
	authorizingUser, err := checkAuthorization(c.Request)
	if err != nil || authorizingUser == nil {
		log.Log.ReturnError(c, http.StatusForbidden, ErrorForbidden, fmt.Sprintf("%v", err), nil)
		return
	}

	id, err := utils.StringToUint(c.Query("id"))
	if err != nil {
		log.Log.ReturnError(c, http.StatusBadRequest, ErrorInput, err.Error(), nil)
		return
	}
	ban_reason, err := utils.StringToUint(c.Query("ban_reason"))
	if err != nil {
		log.Log.ReturnError(c, http.StatusBadRequest, ErrorInput, err.Error(), nil)
		return
	}
	if ban_reason > u.BanReasonAdministrative {
		msg := "Ошибка в параметре ban_reason"
		log.Log.ReturnError(c, http.StatusBadRequest, ErrorInput, msg, nil)
		return
	}

	user, err := tryToLoadUser(c, id, nil)
	if err != nil {
		return
	}

	user.ToggleBanned(&ban_reason)
	if err := user.VSave(); err != nil {
		log.Log.ReturnError(c, http.StatusInternalServerError, err.Code(), err.Message(), nil)
		return
	}

	c.Status(http.StatusOK)
}

// apiChangePasswordInput данные, которые принимает метод apiChangePassword
type apiChangePasswordInput struct {
	Id          uint   `json:"id"`
	OldPassword string `json:"oldPassword"`
	NewPassword string `json:"newPassword"`
}

// apiChangePasswordOutput данные, которые возвращает метод apiChangePassword
type apiChangePasswordOutput apiEditUserOutput

// apiChangePassword обработчик смены пароля
func apiChangePassword(c *gin.Context) {
	authorizingUser, err := checkAuthorization(c.Request)
	if err != nil || authorizingUser == nil {
		log.Log.ReturnError(c, http.StatusForbidden, ErrorForbidden, fmt.Sprintf("%v", err), nil)
		return
	}

	var input apiChangePasswordInput
	body, err := readAndUnmarshal(c, &input)
	if err != nil {
		log.Log.ReturnError(c, http.StatusBadRequest, ErrorInput, err.Error(), body)
		return
	}

	user, err := tryToLoadUser(c, input.Id, body)
	if err != nil {
		return
	}

	if !authorizingUser.CanDoWithOther(user.Id) {
		log.Log.ReturnError(c, http.StatusForbidden, ErrorForbidden, "Not enough privileges", nil)
		return
	}

	if user.Password != input.OldPassword {
		log.Log.ReturnError(c, http.StatusBadRequest, ErrorInconsistentData, "Invalid current password", body)
		return
	}

	if input.NewPassword != input.OldPassword {
		user.GenerateToken()
		user.Password = input.NewPassword

		if err := user.VSave(); err != nil {
			log.Log.ReturnError(c, http.StatusInternalServerError, err.Code(), err.Message(), body)
			return
		}
	}

	c.JSON(http.StatusOK, apiChangePasswordOutput{
		Id:    user.Id,
		Token: *user.Token,
	})
}

// apiChangeEmailInput данные, которые принимает метод apiChangeEmail
type apiChangeEmailInput struct {
	Id       uint   `json:"id"`
	Password string `json:"password"`
	NewEmail string `json:"newEmail"`
}

// apiChangeEmailOutput данные, которые возвращает метод apiChangeEmail
type apiChangeEmailOutput apiEditUserOutput

// apiChangeEmail обработчик смены email
func apiChangeEmail(c *gin.Context) {
	authorizingUser, err := checkAuthorization(c.Request)
	if err != nil || authorizingUser == nil {
		log.Log.ReturnError(c, http.StatusForbidden, ErrorForbidden, fmt.Sprintf("%v", err), nil)
		return
	}

	var input apiChangeEmailInput
	body, err := readAndUnmarshal(c, &input)
	if err != nil {
		log.Log.ReturnError(c, http.StatusBadRequest, ErrorInput, err.Error(), body)
		return
	}

	user, err := tryToLoadUser(c, input.Id, body)
	if err != nil {
		return
	}

	if !authorizingUser.CanDoWithOther(user.Id) {
		log.Log.ReturnError(c, http.StatusForbidden, ErrorForbidden, "Not enough privileges", nil)
		return
	}

	if user.Password != input.Password {
		log.Log.ReturnError(c, http.StatusBadRequest, ErrorInconsistentData, "Invalid current password", body)
		return
	}

	if user.Email != input.NewEmail {
		user.GenerateToken()
		user.Email = input.NewEmail

		if err := user.VSave(); err != nil {
			log.Log.ReturnError(c, http.StatusInternalServerError, err.Code(), err.Message(), body)
			return
		}
	}

	c.JSON(http.StatusOK, apiChangeEmailOutput{
		Id:    user.Id,
		Token: *user.Token,
	})
}
