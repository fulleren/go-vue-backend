#! /bin/bash

if [[ $1 == "" ]]; then
	echo "Syntax: $0 up|down|status"
	exit 0
fi

goose -path="goose" $1
