-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE  "user" (
   "id" SERIAL,
   "created_at"   TIMESTAMP NULL DEFAULT NULL,
   "updated_at"   TIMESTAMP NULL DEFAULT NULL,
   "deleted_at"   TIMESTAMP NULL DEFAULT NULL,
   "last_name"   VARCHAR(30) DEFAULT NULL,
   "first_name"   VARCHAR(30) DEFAULT NULL,
   "middle_name"   VARCHAR(30) DEFAULT NULL,
   "sex"    SMALLINT DEFAULT NULL,
   "email"   VARCHAR(50) NOT NULL,
   "birth_date" DATE NULL DEFAULT NULL,

   "role" VARCHAR(20) DEFAULT 'client',

   "phone"   VARCHAR(30) DEFAULT NULL,
   "password"   VARCHAR(255) NOT NULL,
   "token"   VARCHAR(255) DEFAULT NULL,
   "token_expires"   TIMESTAMP WITH TIME ZONE DEFAULT NULL,

   "banned"   BOOLEAN NOT NULL,
   "ban_reason"   INT DEFAULT NULL,

   "session_mins" int NOT NULL DEFAULT 0,
   "timezone" int NOT NULL DEFAULT 0,
   "address" VARCHAR(300) DEFAULT NULL,
   "country"   VARCHAR(100) DEFAULT NULL,
   "region"   VARCHAR(100) DEFAULT NULL,
   "city"   VARCHAR(100) DEFAULT NULL,
   "street"   VARCHAR(255) DEFAULT NULL,
   "house"   VARCHAR(8) DEFAULT NULL,
   "building"   VARCHAR(8) DEFAULT NULL,
   "flat"   VARCHAR(8) DEFAULT NULL,
   "post_code"   VARCHAR(6) DEFAULT NULL,

   "last_ip"   VARCHAR(15) DEFAULT NULL,
   "utm"   VARCHAR(8000) DEFAULT NULL,
   "last_active_date"   TIMESTAMP WITH TIME ZONE DEFAULT NULL,

   primary key ("id"),
   unique ("phone")
);

COMMENT ON COLUMN "user". "created_at" IS 'Создан';
COMMENT ON COLUMN "user". "updated_at" IS 'Обновлен';
COMMENT ON COLUMN "user". "deleted_at" IS 'Удален';
COMMENT ON COLUMN "user". "last_name" IS 'Фамилия';
COMMENT ON COLUMN "user". "first_name" IS 'Имя';
COMMENT ON COLUMN "user". "middle_name" IS 'Отчество';
COMMENT ON COLUMN "user". "sex" IS 'Пол';
COMMENT ON COLUMN "user". "email" IS 'E-mail';
COMMENT ON COLUMN "user". "birth_date" IS 'Дата рождения';
COMMENT ON COLUMN "user". "phone" IS 'Телефон';
COMMENT ON COLUMN "user". "password" IS 'Пароль';
COMMENT ON COLUMN "user". "token" IS 'Токен доступа';
COMMENT ON COLUMN "user". "token_expires" IS 'Время истечения токена доступа';
COMMENT ON COLUMN "user". "banned" IS 'Забанен';
COMMENT ON COLUMN "user". "ban_reason" IS 'Причина блокировки';
COMMENT ON COLUMN "user". "timezone" IS 'Смещение часового пояса';
COMMENT ON COLUMN "user". "session_mins" IS 'Длительность сессии в минутах';
COMMENT ON COLUMN "user". "address" IS 'Адрес целиком';
COMMENT ON COLUMN "user". "country" IS 'Страна';
COMMENT ON COLUMN "user". "region" IS 'Регион';
COMMENT ON COLUMN "user". "city" IS 'Населённый пункт';
COMMENT ON COLUMN "user". "street" IS 'Улица';
COMMENT ON COLUMN "user". "house" IS 'Дом';
COMMENT ON COLUMN "user". "building" IS 'Строение';
COMMENT ON COLUMN "user". "flat" IS 'Квартира';
COMMENT ON COLUMN "user". "post_code" IS 'Почтовый индекс';
COMMENT ON COLUMN "user". "last_ip" IS 'Последний IP адрес';
COMMENT ON COLUMN "user". "utm" IS 'UTM-информация';
COMMENT ON COLUMN "user". "last_active_date" IS 'Дата последней активности';

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE "user" CASCADE;
