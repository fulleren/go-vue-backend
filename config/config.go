package config

import (
	"encoding/json"
	"fmt"
	"net/url"
	"os"
	"strconv"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var (
	db *gorm.DB
)

const (
	RunModeDev  = "dev"
	RunModeProd = "prod"
)

const (
	DriverMysql    = "mysql"
	DriverPostgres = "postgres"
)

const (
	dbConfEnvName = "TEST_GO_VUE_SERVICE_DB_CONF"
	defaultDbConf = `{"driver":"postgres","user":"user","password":"password","host":"",` +
		`"name":"test_go_vue","port":"","charset":"utf8","parseTime":"True","loc":"Europe/Moscow"}`
	runModeEnvName = "TEST_GO_VUE_SERVICE_RUNMODE"
	portEnvName    = "TEST_GO_VUE_SERVICE_PORT"
	defaultPort    = 8999
)

// dbConfig структура для хранения настроек БД
type dbConfig struct {
	Driver    string `json:"driver"`
	Host      string `json:"host"`
	Port      string `json:"port"`
	Password  string `json:"password"`
	User      string `json:"user"`
	Name      string `json:"name"`
	Charset   string `json:"charset"`
	ParseTime string `json:"parseTime"`
	Loc       string `json:"loc"`
}

// normalize выставляет дефолтные значения некоторых параметров
func (dbc *dbConfig) normalize() {
	switch dbc.Driver {
	case DriverMysql:
		if dbc.Host == "localhost" {
			dbc.Host = ""
		}
		if dbc.Port == "" {
			dbc.Port = "3306"
		}
	case DriverPostgres:
		if dbc.Host == "" {
			dbc.Host = "localhost"
		}
		if dbc.Port == "" {
			dbc.Port = "5432"
		}
	}
}

// String выдаёт разную строку для подключения к БД в зависимости от значения поля Driver
func (dbc dbConfig) String() string {
	switch dbc.Driver {
	case DriverMysql:
		paramMap := url.Values{
			"charset":   {dbc.Charset},
			"parseTime": {dbc.ParseTime},
			"loc":       {dbc.Loc},
		}
		return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?%s", dbc.User, dbc.Password, dbc.Host, dbc.Port,
			dbc.Name, paramMap.Encode())
	case DriverPostgres:
		return fmt.Sprintf("host=%s port=%s password=%s user=%s dbname=%s sslmode=disable client_encoding=%s",
			dbc.Host, dbc.Port, dbc.Password, dbc.User, dbc.Name, dbc.Charset)
	}
	panic(fmt.Sprintf("dbConfig String() failed, unknown Driver: %s", dbc.Driver))
}

func runMode() string {
	runMode := os.Getenv(runModeEnvName)
	if runMode == "" {
		runMode = RunModeDev
		fmt.Println("RUN_MODE set to: ", runMode)
	}
	return runMode
}

func dbConf() (dbConfig, error) {
	confString := os.Getenv(dbConfEnvName)
	if confString == "" {
		confString = defaultDbConf
		fmt.Println("Using default DbConf value: ", confString)
	}

	var config dbConfig
	err := json.Unmarshal([]byte(confString), &config)
	return config, err
}

func DB() *gorm.DB {
	if db == nil {
		runMode()
		dbConfig, err := dbConf()
		if err != nil {
			panic(err)
		}

		if db != nil {
			return db
		}

		dbConfig.normalize()
		fmt.Println("connecting to:", dbConfig.String())
		dbl, err := gorm.Open(dbConfig.Driver, dbConfig.String())
		if err != nil {
			fmt.Println("panic")
			panic(err)
		}
		db = dbl

		//db.LogMode(true)
		db.SingularTable(true)
	}

	return db
}

// IsModeProd проверяет, сконфигурирован ли сервис на режим Production. Возвращает true или false
func IsModeProd() bool {
	return runMode() == RunModeProd
}

// Port возвращает номер порта сервиса
func Port() int {
	portString := os.Getenv(portEnvName)
	portInt, err := strconv.ParseInt(portString, 10, 32)
	if err != nil {
		return defaultPort
	}
	return int(portInt)
}
