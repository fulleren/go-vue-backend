package user

import (
	"github.com/mihteh/types"
	"github.com/mtfelian/utils"
	x "github.com/mtfelian/validation"
	"regexp"
	"strings"
	"time"
)

var (
	RegexpPhone    = regexp.MustCompile(`^\d{11}$`)
	RegexpPassword = regexp.MustCompile(`^[a-f\d]{32}$`)
	RegexpToken    = regexp.MustCompile(`^[-_\w\d]{64}$`)
	RegexpIP4      = regexp.MustCompile(`^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.` +
		`(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.` +
		`(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.` +
		`(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$`)
)

// validateAuthData проверяет поля, участвующие в авторизации
func (user *User) validateAuthData(v *x.Validation) {
	user.Email = strings.Replace(user.Email, " ", "", -1)
	v.Check(user.Email, x.ValidEmail()).Message("Invalid e-mail")

	v.Check(user.Password, x.Match{RegexpPassword}).Message("Seems password hash is wrong")

	if user.Token != nil {
		v.Check(*user.Token, x.Match{RegexpToken}).Message("Seems token is wrong")
	}
}

// validateBasicData проверяет основные поля модели пользователя
func (user *User) validateBasicData(v *x.Validation) {
	if !utils.IsNil(user.FirstName) {
		user.FirstName = utils.PString(strings.TrimSpace(*user.FirstName))
		v.Check(*user.FirstName, x.MinSize{2}, x.MaxSize{30}).Message("First name length: 2 to 30 characters")
	}
	if !utils.IsNil(user.MiddleName) {
		user.MiddleName = utils.PString(strings.TrimSpace(*user.MiddleName))
		v.Check(*user.MiddleName, x.MinSize{2}, x.MaxSize{30}).Message("Middle name length: 2 to 30 characters")
	}
	if !utils.IsNil(user.LastName) {
		user.LastName = utils.PString(strings.TrimSpace(*user.LastName))
		v.Check(*user.LastName, x.MinSize{2}, x.MaxSize{30}).Message("Last name length: 2 to 30 characters")
	}

	if user.Sex != nil {
		v.Check(*user.Sex, x.Range{x.Min{SexMale}, x.Max{SexFemale}}).Message("Sex should be male or female")
	}

	if user.BirthDate.Valid {
		ago100 := types.ToDate(time.Now().AddDate(-100, 0, 0))
		ago18 := types.ToDate(time.Now().AddDate(-18, 0, 0))
		if user.BirthDate.Before(ago100) || user.BirthDate.After(ago18) {
			v.Error("BirthDate is invalid")
		}
	}

	if !utils.IsNil(user.Phone) {
		user.Phone = utils.PString(regexp.MustCompile(`[^\d]`).ReplaceAllString(*user.Phone, ""))
		v.Check(*user.Phone, x.Match{RegexpPhone}).Message("Phone should consist of 11 digits")
	}

	if user.BanReason != nil {
		v.Check(*user.BanReason, x.Max{BanReasonAdministrative}).Message("Wrong ban reason")
	}
}

// validateServiceFields проверяет служебные поля
func (user *User) validateServiceFields(v *x.Validation) {
	if user.LastIP != nil && *user.LastIP != "" {
		*user.LastIP = regexp.MustCompile(`[^\d.]`).ReplaceAllString(*user.LastIP, "")
		v.Check(*user.LastIP, x.Match{RegexpIP4}).Message("LastIP is invalid")
	}

	if user.UTM != nil && *user.UTM != "" {
		v.Check(*user.UTM, x.MaxSize{8000}).Message("UTM info too big")
	}
}

// validateAddressFields проверяет поля адреса
func (user *User) validateAddressFields(v *x.Validation) {
	if !utils.IsNil(user.Country) {
		v.Check(*user.Country, x.MaxSize{100}).Message("Invalid Country field")
	}
	if !utils.IsNil(user.Region) {
		v.Check(*user.Region, x.MaxSize{100}).Message("Invalid Region field")
	}
	if !utils.IsNil(user.City) {
		v.Check(*user.City, x.MaxSize{100}).Message("Invalid City field")
	}
	if !utils.IsNil(user.Street) {
		v.Check(*user.Street, x.MaxSize{255}).Message("Invalid Street field")
	}
	if !utils.IsNil(user.House) {
		v.Check(*user.House, x.MaxSize{8}).Message("Invalid House field")
	}
	if !utils.IsNil(user.Building) {
		v.Check(*user.Building, x.MaxSize{8}).Message("Invalid Building field")
	}
	if !utils.IsNil(user.Flat) {
		v.Check(*user.Flat, x.MaxSize{8}).Message("Invalid Flat field")
	}
	if !utils.IsNil(user.PostCode) {
		v.Check(*user.PostCode, x.MaxSize{6}).Message("Invalid PostCode field")
	}
	if !utils.IsNil(user.Address) {
		v.Check(*user.Address, x.MaxSize{500}).Message("Invalid Address field")
	}
	v.Check(user.TimeZone, x.Min{-12}, x.Max{12}).Message("Invalid time zone")
}

// Validate валидирует модель пользователя
func (user *User) Validate() (bool, *x.Validation) {
	v := &x.Validation{}

	user.validateAuthData(v)
	user.validateBasicData(v)
	user.validateServiceFields(v)
	user.validateAddressFields(v)
	return !v.HasErrors(), v
}
