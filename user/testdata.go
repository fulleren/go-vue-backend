package user

import (
	"bitbucket.org/fulleren/test-go-vue/config"
	"github.com/mihteh/types"
	"github.com/mtfelian/cli"
	. "github.com/mtfelian/utils"
)

type TestData struct {
	Users Users
}

var TD TestData

// GetValidUserMinimal возвращает минимальную валидную модель пользователя
func GetValidUserMinimal() User {
	return User{
		Email:        "ab@ab.ru",
		Password:     "098f6bcd4621d373cade4e832627b4f6", // md5 от "test"
		LastIP:       PString("127.0.0.1"),
		dbConnection: config.DB(),
	}
}

// GetValidUser возвращает валидную модель пользователя
func GetValidUser() User {
	return User{
		Role: RoleClient,

		FirstName:  PString("Vasya"),
		LastName:   PString("Shmelev"),
		MiddleName: PString("Евстафович"),
		Sex:        PUint(SexMale),
		BirthDate:  types.MakeNullDate(),
		Email:      "ab@ab.ru",

		Phone:        PString("+71234567980"),            // валидация вырежет всё кроме цифр
		Password:     "098f6bcd4621d373cade4e832627b4f6", // md5 от "test"
		Token:        nil,
		TokenExpires: types.MakeNullDateTime(),

		Banned:    false,
		BanReason: nil,

		Country:     nil,
		Region:      nil,
		City:        nil,
		Street:      nil,
		House:       nil,
		Building:    nil,
		Flat:        nil,
		PostCode:    nil,
		Address:     nil,
		TimeZone:    3,
		SessionMins: 60,

		LastIP:         PString("127.0.0.1"),
		UTM:            nil,
		LastActiveDate: types.DateTimeNow().Nullable(),

		dbConnection: config.DB(),
	}
}

// Cleanup удаляет данные из БД
func Cleanup() {
	TD = TestData{Users: Users{}}
	if err := HardDeleteAll(); err != nil {
		cli.Println("{YCleanup Error: %v{0", err)
	}
}

// Generate создаёт тестовые данные в БД
func Generate() {
	userClient := GetValidUser()
	userClient.Phone = PString("+71234567980")
	userClient.Role = RoleClient
	userClient.GenerateToken()

	if err := userClient.VSave(); err != nil {
		cli.Println("{YGenerate Error: %v{0", err)
	}

	userAdmin := GetValidUser()
	userAdmin.Phone = PString("+71234567981")
	userAdmin.Role = RoleAdmin
	userAdmin.GenerateToken()

	if err := userAdmin.VSave(); err != nil {
		cli.Println("{YGenerate Error: %v{0", err)
	}

	userSystem := GetValidUser()
	userSystem.Phone = PString("+71234567982")
	userSystem.Role = RoleSystem
	userSystem.GenerateToken()

	if err := userSystem.VSave(); err != nil {
		cli.Println("{YGenerate Error: %v{0", err)
	}

	TD.Users = append(TD.Users, userClient, userAdmin, userSystem)
}
