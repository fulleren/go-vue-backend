package user

import (
	"bitbucket.org/fulleren/test-go-vue/config"
	. "bitbucket.org/fulleren/test-go-vue/errors"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/kr/pretty"
	"github.com/mihteh/types"
	. "github.com/mtfelian/error"
	"github.com/mtfelian/utils"
	"reflect"
	"time"
)

const (
	OrderDirectionDescending = "DESC" // сортировка по убыванию
	OrderDirectionAscending  = "ASC"  // сортировка по возрастанию
)

const (
	RoleNone   = ""       // нет роли
	RoleClient = "client" // клиент
	RoleAdmin  = "admin"  // администратор
	RoleSystem = "system" // система
)

const (
	SexMale   = iota + 1 // мужской
	SexFemale            // женский
)

const (
	BanReasonNone           = iota // неизвестная причина
	BanReasonNotActivated          // не активирован
	BanReasonSelfDisabled          // заблокировал свой аккаунт
	BanReasonFraud                 // мошенник
	BanReasonAdministrative        // заблокирован администратором
)

// TokenLifeTime задаёт срок жизни токена доступа
const TokenLifeTime = 6 * time.Hour

// User это модель, описывающая сущность "Пользователь"
type User struct {
	Id        uint       `gorm:"column:id" json:"id"`
	CreatedAt time.Time  `gorm:"column:created_at" json:"createdAt"`
	UpdatedAt time.Time  `gorm:"column:updated_at" json:"updatedAt"`
	DeletedAt *time.Time `gorm:"column:deleted_at" json:"deletedAt"`

	Role string `gorm:"column:role" json:"role"`

	FirstName  *string        `gorm:"column:first_name" json:"firstName,omitempty"`
	LastName   *string        `gorm:"column:last_name" json:"lastName,omitempty"`
	MiddleName *string        `gorm:"column:middle_name" json:"middleName,omitempty"`
	Sex        *uint          `gorm:"column:sex" json:"sex,omitempty"`
	BirthDate  types.NullDate `gorm:"column:birth_date" json:"birthDate,omitempty"`
	Email      string         `gorm:"column:email" json:"email"`

	Phone        *string            `gorm:"column:phone" json:"phone,omitempty"`
	Password     string             `gorm:"column:password" json:"password,omitempty"`
	Token        *string            `gorm:"column:token" json:"token,omitempty"`
	TokenExpires types.NullDateTime `gorm:"column:token_expires" json:"-"`

	Banned    bool  `gorm:"column:banned" json:"banned"`
	BanReason *uint `gorm:"column:ban_reason" json:"banReason,omitempty"`

	// поля адреса
	Country     *string `gorm:"column:country" json:"country,omitempty"`
	Region      *string `gorm:"column:region" json:"region,omitempty"`
	City        *string `gorm:"column:city" json:"city,omitempty"`
	Street      *string `gorm:"column:street" json:"street,omitempty"`
	House       *string `gorm:"column:house" json:"house,omitempty"`
	Building    *string `gorm:"column:building" json:"building,omitempty"`
	Flat        *string `gorm:"column:flat" json:"flat,omitempty"`
	PostCode    *string `gorm:"column:post_code" json:"postCode,omitempty"`
	Address     *string `gorm:"column:address" json:"address,omitempty"`
	TimeZone    int     `gorm:"column:timezone" json:"timezone"`
	SessionMins uint    `gorm:"column:session_mins" json:"sessionMins,omitempty"`

	// служебные поля
	LastIP         *string            `gorm:"column:last_ip" json:"lastIp,omitempty"`
	UTM            *string            `gorm:"column:utm" json:"utm,omitempty"`
	LastActiveDate types.NullDateTime `gorm:"column:last_active_date" json:"lastActiveDate,omitempty"`

	// поля, не хранимые в БД
	UnencryptedPassword string   `sql:"-" json:"-"` // незашифрованный пароль, до создания
	dbConnection        *gorm.DB `sql:"-" json:"-"`
}

// Users - пользователи
type Users []User

// TableName задаёт имя таблицы для связи с моделью
func (user User) TableName() string {
	return "user"
}

// SetLastActiveDate устанавливает дату последней активности в "сейчас"
func (user *User) SetLastActiveDate() {
	user.LastActiveDate = types.DateTimeNow().Nullable()
}

// ProlongToken продляет срок действия токена доступа на стандартное время жизни токена доступа
func (user *User) ProlongToken() {
	user.TokenExpires = types.ToDateTime(time.Now().Add(TokenLifeTime)).Nullable()
}

// GenerateToken формирует токен доступа для пользователя и записывает в соотв-е поле
func (user *User) GenerateToken() {
	if user.Role == RoleSystem && !utils.IsNil(user.Token) { // todo костыль с токеном для RoleSystem
		return
	}

	s, err := utils.UniqID(64)
	if err != nil {
		fmt.Println("Error from UniqID(): ", err)
	}
	s = fmt.Sprintf("%d%s", user.Id, s)[:64]
	user.Token = utils.PString(s)
	user.ProlongToken()
}

// initDBConnection инициализирует соединение модели с БД
// если оно ещё не было проинициализировано
func (user *User) initDBConnection() *gorm.DB {
	if user.dbConnection == nil {
		user.SetDBConnection(config.DB())
	}
	return user.dbConnection
}

// SetDBConnection устанавливает свойство dbConnection
func (user *User) SetDBConnection(connection *gorm.DB) {
	user.dbConnection = connection
}

// Save валидирует, а затем сохраняет пользователя в БД
func (user *User) VSave() Error {
	isValid, vContext := user.Validate()
	if !isValid {
		return NewErrorf(ErrorValidation, vContext.String())
	}
	return user.Save()
}

// Save сохраняет пользователя в БД
func (user *User) Save() Error {
	dbConnection := user.initDBConnection()
	if user.Id > 0 {
		return MayError(ErrorSaving, dbConnection.Save(user).Error)
	}
	return MayError(ErrorSaving, dbConnection.Create(user).Error)
}

// LoadById загружает пользователя по id
func LoadById(id uint) (*User, error) {
	dbConnection := config.DB()
	return LoadByIdWithConnection(id, dbConnection)
}

// Reload перезагружает данные модели
func (user *User) Reload() error {
	loadedUser, err := LoadById(user.Id)
	if err == nil {
		*user = *loadedUser
	}
	return err
}

// LoadByIdWithConnection загружает пользователя по id через заданное соединение
func LoadByIdWithConnection(id uint, dbConnection *gorm.DB) (*User, error) {
	var user User
	err := dbConnection.Where("id = ?", id).First(&user).Error
	if err == gorm.ErrRecordNotFound {
		return nil, nil
	}
	user.SetDBConnection(dbConnection)
	return &user, err
}

// LoadByEmail загружает пользователя по логину (email)
func LoadByEmail(email string) (*User, error) {
	dbConnection := config.DB()
	var user User
	err := dbConnection.Where("email = ?", email).First(&user).Error
	if err == gorm.ErrRecordNotFound {
		return nil, nil
	}
	return &user, err
}

// IsExistsByEmail проверяет существование пользователя с указанным email
func IsExistsByEmail(email string) (bool, error) {
	var count uint
	dbConnection := config.DB()
	err := dbConnection.Model(User{}).Where("email = ?", email).Count(&count).Error
	return count > 0, err
}

// LoadByToken загружает пользователя по токену доступа, если он не истёк
func LoadByToken(token string) (*User, error) {
	dbConnection := config.DB()
	var user User
	err := dbConnection.Where("token = ?", token).First(&user).Error
	// todo костыль с токеном для RoleSystem // убрал условие AND token_expires > now()
	notExpiredRoleSystem := user.Role != RoleSystem && user.TokenExpires.Before(types.DateTimeNow())
	if err == gorm.ErrRecordNotFound || notExpiredRoleSystem {
		return nil, nil
	}
	return &user, err
}

// HardDelete удаляет запись из БД
func (user *User) HardDelete() error {
	dbConnection := user.initDBConnection()
	return dbConnection.Unscoped().Delete(user).Error
}

// HardDeleteAll удаляет все записи из БД
func HardDeleteAll() error {
	dbConnection := config.DB()
	return dbConnection.Unscoped().Where(true).Delete(User{}).Error
}

// UpdateWith заменяет в БД данные пользователя user пользователем replacement, сохраняя id, email и role
func (user *User) UpdateWith(replacement User, validate bool) error {
	user.FirstName = replacement.FirstName
	user.LastName = replacement.LastName
	user.MiddleName = replacement.MiddleName
	user.Sex = replacement.Sex
	user.BirthDate = replacement.BirthDate
	user.Phone = replacement.Phone
	user.Country = replacement.Country
	user.Region = replacement.Region
	user.City = replacement.City
	user.Street = replacement.Street
	user.House = replacement.House
	user.Building = replacement.Building
	user.Flat = replacement.Flat
	user.PostCode = replacement.PostCode
	user.Address = replacement.Address
	user.TimeZone = replacement.TimeZone
	user.SessionMins = replacement.SessionMins
	if validate {
		return user.VSave()
	}
	return user.Save()
}

// ToggleBanned изменяет значение флага Banned на противоположное и устанавливает
// причину блокировки, переданную в параметре ban_reason, если происходит блокировка.
// Если же происходит разблокировка, причина блокировки обнуляется и параметр ban_reason
// тогда не играет роли
func (user *User) ToggleBanned(banReason *uint) {
	user.Banned = !user.Banned
	user.BanReason = nil
	if user.Banned {
		user.BanReason = banReason
	}
}

// IsAdmin возвращает true если пользователь является Администратором, иначе false
func (user User) IsAdmin() bool {
	return user.Role == RoleAdmin
}

// CanDoWithOther возвращает true если пользователь имеет права применять методы к другим пользователям,
// то есть, является админом или применяет метод к самому себе, иначе возвращает false
func (user User) CanDoWithOther(otherUserId uint) bool {
	return user.IsAdmin() || user.Id == otherUserId
}

// Compare сравнивает две модели. Возвращает:
// bool - true если равны, иначе false
// string - список расхождений по полям в читаемом виде
func Compare(a, b User) (bool, string) {
	a.dbConnection, b.dbConnection = nil, nil
	a.CreatedAt, b.CreatedAt = time.Time{}, time.Time{}
	a.UpdatedAt, b.UpdatedAt = time.Time{}, time.Time{}
	if !reflect.DeepEqual(a, b) {
		return false, fmt.Sprint(utils.StringSlice(pretty.Diff(a, b)))
	}
	return true, ""
}

// CompareExceptSomeFields сравнивает две модели без учёта полей Password, Token, TokenExpires, UTM, Credit, Fish
// Возвращает:
// bool - true если равны, иначе false
// string - список расхождений по полям в читаемом виде
func CompareExceptSomeFields(a, b User) (bool, string) {
	a.dbConnection, b.dbConnection = nil, nil
	a.CreatedAt, b.CreatedAt = time.Time{}, time.Time{}
	a.UpdatedAt, b.UpdatedAt = time.Time{}, time.Time{}
	a.Password, b.Password = "", ""
	a.Token, b.Token = nil, nil
	a.TokenExpires, b.TokenExpires = types.MakeNullDateTime(), types.MakeNullDateTime()
	a.UTM, b.UTM = nil, nil
	if !reflect.DeepEqual(a, b) {
		return false, fmt.Sprint(utils.StringSlice(pretty.Diff(a, b)))
	}
	return true, ""
}

// LoadList загружает список всех пользователей начиная с позиции from и заканчивая to
func LoadList(from, to uint) (Users, error) {
	users := Users{}
	dbConnection := config.DB()
	query := dbConnection.Model(User{}).Where("true").
		Order("id DESC").Limit(to - from + 1).Offset(from - 1).Scan(&users)
	return users, query.Error
}
