package user

import (
	"github.com/mihteh/types"
	. "github.com/mtfelian/utils"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"strings"
	"time"
)

var _ = Describe("Checking User model.", func() {
	It("checks TableName", func() {
		expectedTableName := "user"
		receivedTableName := (User{}).TableName()
		Expect(receivedTableName).To(Equal(expectedTableName))
	})

	It("checks loading and VSave", func() {
		savedUser := GetValidUser()
		Expect(savedUser.VSave()).To(Succeed())
		defer savedUser.HardDelete()

		loadedUser, err := LoadById(savedUser.Id)
		Expect(err).NotTo(HaveOccurred())
		Expect(loadedUser).ToNot(BeNil())

		equal, diff := Compare(savedUser, *loadedUser)
		Expect(equal).To(BeTrue(), diff)
	})

	It("checks VSave for invalid user", func() {
		savedUser := GetValidUser()
		savedUser.FirstName = PString(strings.Repeat("a", 50))
		err := savedUser.VSave()
		Expect(err).To(HaveOccurred())
		defer HardDeleteAll()
	})

	It("Checks loading by Id if user not exists", func() {
		user, err := LoadById(0)
		Expect(err).NotTo(HaveOccurred())
		Expect(user).To(BeNil())
	})

	It("checks IsExistsByEmail if not exists", func() {
		isExists, err := IsExistsByEmail("qq")
		Expect(err).NotTo(HaveOccurred())
		Expect(isExists).To(BeFalse())
	})

	It("checks loading by email if not exists", func() {
		user, err := LoadByEmail("something")
		Expect(err).NotTo(HaveOccurred())
		Expect(user).To(BeNil())
	})

	It("checks token creation", func() {
		user := User{}
		user.GenerateToken()
		Expect(user.Token).ToNot(BeNil())
		Expect(*user.Token).To(MatchRegexp(RegexpToken.String()))
	})

	Context("with DB Generate and Cleanup", func() {
		AfterEach(func() { Cleanup() })
		BeforeEach(func() { Generate() })

		It("checks loading user by email", func() {
			expectedEmail := TD.Users[0].Email
			user, err := LoadByEmail(expectedEmail)
			Expect(err).NotTo(HaveOccurred())
			Expect(user.Email).To(Equal(expectedEmail))
		})

		It("checks loading list of users", func() {
			expectedLength := len(TD.Users)
			users, err := LoadList(1, 100500)
			Expect(err).NotTo(HaveOccurred())
			receivedLength := len(users)
			Expect(receivedLength).To(Equal(expectedLength))
		})

		It("checks IsExistsByEmail", func() {
			isExists, err := IsExistsByEmail(TD.Users[0].Email)
			Expect(err).NotTo(HaveOccurred())
			Expect(isExists).To(BeTrue())
		})

		It("checks loading by token", func() {
			TD.Users[0].GenerateToken()
			Expect(TD.Users[0].Save()).To(Succeed())

			expectedToken := *TD.Users[0].Token
			user, err := LoadByToken(expectedToken)
			Expect(err).NotTo(HaveOccurred())
			Expect(*user.Token).To(Equal(expectedToken))
		})

		It("checks loading by token if it expired", func() {
			TD.Users[0].GenerateToken()
			TD.Users[0].TokenExpires = types.ToDateTime(time.Now().Add(-time.Minute)).Nullable()
			Expect(TD.Users[0].Save()).To(Succeed())

			user, err := LoadByToken(*TD.Users[0].Token)
			Expect(err).NotTo(HaveOccurred())
			Expect(user).To(BeNil())
		})

	})

	It("checks loading by token if not exists", func() {
		user, err := LoadByToken("something")
		Expect(err).NotTo(HaveOccurred())
		Expect(user).To(BeNil())
	})

	It("checks IsAdmin", func() {
		user := GetValidUser()
		Expect(user.IsAdmin()).To(BeFalse())

		user.Role = RoleAdmin
		Expect(user.IsAdmin()).To(BeTrue())
	})

})
