package user

import (
	. "github.com/kr/pretty"
	"github.com/mihteh/types"
	. "github.com/mtfelian/utils"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"strings"
	"time"
)

var _ = Describe("Checking User Validation.", func() {

	It("checks validation in different cases", func() {
		user := GetValidUser()

		type invalidateFunc func(user *User)
		pFunc := func(f invalidateFunc) *invalidateFunc { return &f }
		type result struct {
			isValid    bool
			errorCount int
		}
		testCases := map[*invalidateFunc]result{
			pFunc(func(user *User) {}):                                                                            {true, 0},
			pFunc(func(user *User) { user.FirstName = PString("a") }):                                             {false, 1},
			pFunc(func(user *User) { user.FirstName = PString(strings.Repeat("a", 31)) }):                         {false, 1},
			pFunc(func(user *User) { user.LastName = PString("a") }):                                              {false, 1},
			pFunc(func(user *User) { user.LastName = PString(strings.Repeat("a", 31)) }):                          {false, 1},
			pFunc(func(user *User) { user.MiddleName = PString("a") }):                                            {false, 1},
			pFunc(func(user *User) { user.MiddleName = PString(strings.Repeat("a", 31)) }):                        {false, 1},
			pFunc(func(user *User) { user.Sex = PUint(SexFemale + 1) }):                                           {false, 1},
			pFunc(func(user *User) { user.Sex = PUint(SexMale - 1) }):                                             {false, 1},
			pFunc(func(user *User) { user.BirthDate = types.ToDate(time.Now().AddDate(-18, 0, 1)).Nullable() }):   {false, 1},
			pFunc(func(user *User) { user.BirthDate = types.ToDate(time.Now().AddDate(-18, 0, 0)).Nullable() }):   {true, 0},
			pFunc(func(user *User) { user.BirthDate = types.ToDate(time.Now().AddDate(-100, 0, -1)).Nullable() }): {false, 1},
			pFunc(func(user *User) { user.Email = "" }):                                                           {false, 1},
			pFunc(func(user *User) { user.Email = "a@a@a.ru" }):                                                   {false, 1},
			pFunc(func(user *User) { user.Email = "a.ru" }):                                                       {false, 1},
			pFunc(func(user *User) { user.Phone = PString("1234567890") }):                                        {false, 1},
			pFunc(func(user *User) { user.Phone = PString("123456789012") }):                                      {false, 1},
			pFunc(func(user *User) { user.BanReason = PUint(BanReasonAdministrative + 1) }):                       {false, 1},
			pFunc(func(user *User) { user.Password = "" }):                                                        {false, 1},
			pFunc(func(user *User) { user.Password = "куку" }):                                                    {false, 1},
			pFunc(func(user *User) { user.Token = PString("куку") }):                                              {false, 1},
			pFunc(func(user *User) { user.GenerateToken() }):                                                      {true, 0},
			pFunc(func(user *User) { user.TimeZone = 13 }):                                                        {false, 1},
			pFunc(func(user *User) { user.LastIP = PString("0.0.0.0.0") }):                                        {false, 1},
			pFunc(func(user *User) { user.UTM = PString(strings.Repeat("a", 8001)) }):                             {false, 1},
			pFunc(func(user *User) { user.UTM = PString(strings.Repeat("a", 8000)) }):                             {true, 0},

			// bug with empty optional field as invalid
			pFunc(func(user *User) { user.MiddleName = PString("") }): {true, 0},
		}

		for pointerToFunction, expectedResult := range testCases {
			testCaseUser := user
			invalidate := *pointerToFunction
			invalidate(&testCaseUser)
			isValid, vContext := testCaseUser.Validate()
			receivedResult := result{isValid, len(vContext.Errors)}
			diff := strings.Join(Diff(receivedResult, expectedResult), "\n")
			Expect(receivedResult).To(Equal(expectedResult), diff)
		}
	})

})
