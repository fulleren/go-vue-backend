package main

import (
	"fmt"
	"github.com/mtfelian/log"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"net/http/httptest"
	"os"
	"testing"
)

var (
	server *httptest.Server
)

func TestMain(m *testing.M) {
	server = httptest.NewServer(Gin())
	defer server.Close()

	var err error
	RBAC, err = initRBAC()
	if err != nil {
		fmt.Sprintf("Error init roles: %v\n", err)
		os.Exit(1)
		return
	}

	log.Log, err = log.InitLog()
	if err != nil {
		fmt.Println("Failed to start logger: ", err.Error())
	}

	os.Exit(m.Run())
}

func TestUserService(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Main Suite")
}
