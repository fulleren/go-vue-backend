#! /bin/bash

IFS=' ' read -r -a golist <<< `go list ./...`;for package in "${golist[@]}"; do go test -v $package | grep FAIL ; done
